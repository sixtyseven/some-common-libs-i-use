(function(window) {

   // Events constructor
   var Events = function() {
       // Container for storing event related information:
       // handlers, scopes, event eventNames
       this.__events = [];
   };

   // All eventing related methods are public
   Events.prototype = {

       bind: function(eventName, eventHandler, handlerScope) {
           // Event handler MUST be a function
           if (!(this.__events[eventName] instanceof Array)) {
               this.__events[eventName] = [];
           }
           this.__events[eventName].push({
               eventHandler: eventHandler,
               handlerScope: handlerScope || window
           });
           return this; // for chaining
       },

       unbind: function(eventName, eventHandler, handlerScope) {
           var index;
           if (!(this.__events[eventName] instanceof Array)) {
               return;
           }

           for (index = 0; index < this.__events[eventName].length; index++) {
               if (this.__events[eventName][index].eventHandler === eventHandler &&
                   this.__events[eventName][index].handlerScope === handlerScope) {
                   this.__events[eventName].splice(index, 1);
                   return;
               }
           }
           return this; // for chaining
       },

       fire: function(eventName, handlerArguments) {
           var index;

           if (!(this.__events[eventName] instanceof Array)) {
               return;
           }

           if (!(handlerArguments instanceof Array)) {
               handlerArguments = [handlerArguments];
           }

           for (index = 0; index < this.__events[eventName].length; index++) {
               this.__events[eventName][index].eventHandler.apply(this.__events[eventName][index].handlerScope, handlerArguments);
           }
           return this; // for chaining
       }
   };

   var pubsub = new Events;

})(window);
